# pull official base image
FROM python:3.9.0-slim

# set work directory
WORKDIR /usr/src/app

# set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# ENV below will be overrided by .env PYTHONPATH when running docker-compose
# But it allows to run image indepedently
ENV PYTHONPATH=${PYTHONPATH}:/usr/src/app/app:app

COPY ./requirements.txt /usr/src/app/requirements.txt
RUN pip install -r requirements.txt

# copy project
RUN mkdir -p /usr/src/app/static

COPY app /usr/src/app/app

EXPOSE 8000
CMD uvicorn app.main:app --host 0.0.0.0