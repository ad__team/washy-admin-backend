# README #

### Environment setup
- python 3.9
- pyenv (dependency manager, easy-to-use virtual envs manager)
- pipenv [optional] (python versions installation, alternative virtualenv manager) [Prerequisites](https://github.com/pyenv/pyenv/wiki/Common-build-problems)


```shell
# example

$ pyenv install 3.9.0
$ pipenv shell --python ~/.pyenv/versions/3.9.0/bin/python
$ pipenv install --dev

# start server
$ uvicorn app.main:app --reload
```

### Tests
- To run tests simply run `pytest` (currently pytest is added as developer dependency)

### Maintenance things

*requirements.py* - this file is only used by Docker, any project dependencies should be added using *pipenv*. Calling **_pip freeze >> requirements.txt_** will result with dev-polluted entries. In order to generate 'clean' requirements file run `pipfile2req >> requirements.txt` 