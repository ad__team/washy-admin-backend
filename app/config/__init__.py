from os import environ
from collections import defaultdict

from config import dev, production


def get_settings():
    env = environ.get('env', 'dev').lower()

    settings_dict = defaultdict(
        lambda: dev,
        {
            'dev': dev,
            'production': production
        }
    )
    return settings_dict[env].Settings()


settings = get_settings()
