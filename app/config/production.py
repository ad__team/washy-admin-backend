import config.base


class Settings(config.base.Settings):
    env: str = "production"
