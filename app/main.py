from functools import lru_cache

from fastapi import FastAPI

import config
from db import db as elo
from models.carwash import Carwash
from models.service import Service
from models.user import User
from models.user_order import UserOrder


@lru_cache
def get_settings():
    return config.settings


app = FastAPI()


@app.get('/')
async def carwashes_list() -> list[Carwash]:
    return [Carwash.from_mongo(x) async for x in elo.carwashes.find()]


@app.post('/')
async def add_carwash(carwash: Carwash):
    res = await elo.carwashes.insert_one(carwash.mongo())
    found = await elo.carwashes.find_one({'_id': res.inserted_id})
    return Carwash.from_mongo(found)


@app.post('/users')
async def add_user(user: User):
    res = await elo.users.insert_one(user.mongo())
    found = await elo.users.find_one({'_id': res.inserted_id})
    return User.from_mongo(found)


@app.get('/users')
async def user_list() -> list[User]:
    return [User.from_mongo(x) async for x in elo.users.find()]


@app.post('/orders')
async def add_order(order: UserOrder):
    res = await elo.user_orders.insert_one(order.mongo())
    found = await elo.user_orders.find_one({'_id': res.inserted_id})
    return UserOrder.from_mongo(found)


@app.get('/orders')
async def order_list() -> list[UserOrder]:
    return [UserOrder.from_mongo(x) async for x in elo.user_orders.find()]


@app.post('/services')
async def add_service(service: Service):
    res = await elo.services.insert_one(service.mongo())
    found = await elo.services.find_one({'_id': res.inserted_id})
    return Service.from_mongo(found)


@app.get('/services')
async def service_list() -> list[Service]:
    return [Service.from_mongo(x) async for x in elo.services.find()]