from typing import Optional

from models import MongoModel


class Carwash(MongoModel):
    name: str
    description: Optional[str] = None
    logo: Optional[str] = None

    class Config:
        schema_extra = {
            "example": {
                "name": "WashCo",
                "description": "American Carwash with traditions",
                "logo": "https://i.imgur.com/XtZDBGG.jpg"
            }
        }
