import datetime
from enum import Enum
from typing import Optional, Union

from pydantic import Field

from models import MongoModel
from models.carwash import Carwash
# from models.service import Service


class Group(MongoModel):
    title: str = Field(max_length=200)
    description: str = Field(max_length=250)
    carwash: Carwash
    # services: list[Service]