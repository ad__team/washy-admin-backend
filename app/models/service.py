import datetime
from enum import Enum
from typing import Optional, Union

from pydantic import Field

from models import MongoModel
from models.carwash import Carwash
from models.group import Group


class Service(MongoModel):
    title: str = Field(max_length=200)
    description: str = Field(max_length=200)
    price: float
    enabled: bool = False
    # name to times_taken? defualt 0?
    time_taken: int = Field(default=0, gte=0)
    # default 1, gte1?
    order: int = Field(default=1, gte=1)
    carwash: Carwash

    # class Config:
    #     schema_extra = {
    #         "example": {
    #             "title": "Gold Package",
    #             "description": "Interior + exterior with ultra waxing",
    #             "price": 123,
    #             "enabled": True,
    #             "time_taken": 123,
    #             "order": 997,
    #             "carwash": Carwash,
    #             # "group": Group
    #         }
    #     }
