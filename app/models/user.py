import datetime
from enum import Enum
from typing import Optional, Union

from pydantic import EmailStr, Field

from models import MongoModel
# from models.car import Car


class UserStatusEnum(str, Enum):
    ACTIVE = 'active'
    BANNED = 'banned'
    IN_REVIEW = 'in_review'


class User(MongoModel):
    email: EmailStr
    name: str = Field(..., max_length=200)
    password: Optional[str] = None
    avatar: Union[str, None] = None
    facebook_id: Union[str, None] = None
    google_id: Union[str, None] = None
    status: UserStatusEnum = UserStatusEnum.ACTIVE
    joined: datetime.datetime = datetime.datetime.now()
    # cars: list[Car] = []
    marketing_consent: bool = False
    rodo: bool = False

    class Config:
        schema_extra = {
            "example": {
                "email": "j.c@gmail.com",
                "name": "Jonny Cache",
                "password": "",
                "avatar": "",
                "facebook_id": "123",
                "google_id": "123",
                "status": UserStatusEnum.ACTIVE,
                "jonied": datetime.datetime.now(),
                # "cars": [],
                "marketing_consent": True,
                "rodo": True,
            }
        }
