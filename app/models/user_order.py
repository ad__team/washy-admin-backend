import datetime
from enum import Enum
from typing import Optional, Union

from pydantic import Field

from models import MongoModel
from models.carwash import Carwash
from models.service import Service
from models.user import User


class OrderStateEnum(str, Enum):
    UPCOMING = 'upcoming'
    REMOVED = 'removed'
    DONE = 'done'


class UserOrder(MongoModel):
    service: list[Service] = []
    when: datetime.datetime
    creatd: datetime.datetime = datetime.datetime.now()
    carwash: Carwash
    user: User
    state: OrderStateEnum = OrderStateEnum.UPCOMING
