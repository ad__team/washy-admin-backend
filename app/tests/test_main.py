from fastapi.testclient import TestClient

from main import app


client = TestClient(app)


def test_home_page():
    response = client.get("/")
    # epic asserts
    assert response.status_code == 200
    assert '<img src="https://i.imgur.com/Uf2k7iH.png"/img>' in response.content.decode('utf-8')